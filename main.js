let interval = 3000;
let time = interval;
let tick = 10;
let timer;

const start = () => {
  timer = setInterval(() => {
    time -= tick;
    if (time <= 0) {
      time = interval;
      changeImage();
    }
    showTime(time);
  }, tick);
};

const stop = () => {
  clearInterval(timer);
};

const images = document.querySelectorAll(".images-wrapper .image-to-show");
let index = 0;
let currentImage = images[index];
const getNextImage = () => {
  if (index >= images.length - 1) index = 0;
  else ++index;
  return images[index];
};

const changeImage = () => {
  currentImage.classList.remove("show");
  currentImage = getNextImage();
  currentImage.classList.add("show");
};

const timeDisplay = document.querySelector("#time");
const showTime = (time) => {
  const sec = Math.floor(time / 1000);
  const msec = time - sec * 1000;
  timeDisplay.innerText = `${sec} sec ${msec} msec`;
};

const slider = ({ target, currentTarget }) => {
  const button = target.closest("button");
  if (!button) return;
  currentTarget.querySelectorAll("button").forEach((button) => {
    button.disabled = "";
  });
  const action = button.dataset.action;
  switch (action) {
    case "start":
      start();
      button.disabled = "disabled";
      break;
    case "stop":
      stop();
      button.disabled = "disabled";
      break;
  }
};

const controls = document.querySelector(".controls");
controls.addEventListener("click", slider);

start();
